﻿using System;
using System.Collections.Generic;

namespace LagrangeInterpolation
{
    class LagrangeInterpolation
    {
         static List<double> Convolve(List<double> x, List<double> y)
        {
            List<double> wynik = new List<double>();

            for (int i = 1; i < x.Count + y.Count; i++)
            {
                double tmp = 0;
                for (int j = 0; j < i; j++)
                {
                    if ((j > x.Count - 1) || ((i - j - 1) > y.Count - 1))
                    {
                        continue;
                    }
                    tmp += x[j] * y[i - j - 1];
                }
                wynik.Add(tmp);
            }

            return wynik;
        }

        static List<double> CalculateInterpolation(List<double> x, List<double> y)
        {
            List<double> result = new List<double>();
            for (int i = 0; i < x.Count; i++)
            {
                result.Add(0);
            }

            for (int i = 0; i < x.Count; i++)
            {
                List<double> arrayList = new List<double>();
                arrayList.Add(1);

                for (int j = 0; j < x.Count; j++)
                {
                    if (j != i)
                    {
                        List<double> tmp = new List<double> { 1, -x[j] };
                        arrayList = Convolve(arrayList, tmp);

                        for (int n = 0; n < arrayList.Count; n++)
                        {
                            arrayList[n] = arrayList[n] / (x[i] - x[j]);
                        }
                    }
                }
                for (int m = 0; m < arrayList.Count; m++)
                {
                    result[m] += y[i] * arrayList[m];
                }
            }
            return result;
        }

        static void Main(string[] args)
        {
            List<double> x = new List<double>();
            x.Add(2.11);
            x.Add(2.16);
            x.Add(2.22);
            x.Add(2.30);
            x.Add(2.35);
            x.Add(2.42);
            //x.Add(2.47);
            //x.Add(2.52);
            List<double> y = new List<double>();
            y.Add(4.27418);
            y.Add(4.02513);
            y.Add(3.79336);
            y.Add(3.562030);
            y.Add(3.44926);
            y.Add(2.14056);
            //y.Add(2.00897);
            //y.Add(1.86431);
            List<double> coefficient = CalculateInterpolation(x, y);

            foreach (var i in coefficient)
            {
                Console.WriteLine(i);
            }
        }
    }
}